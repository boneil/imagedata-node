var ExifImage = require('exif').ExifImage;
var moment = require('moment');
var colors = require('colors');
var file = require('file');
var fs = require('fs');
var async = require('async');
var path = require('path');
var argv = require('minimist')(process.argv.slice(2));

var months = [ "Janurary", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
var supportedFileExtensions = ['jpg', 'jpeg', 'mp4', 'mov', 'test'];

var sourcePath = //'/Volumes/Pictures/2001/';
  '/users/boneil/Desktop/Picture_in1';
var destinationPath = '/users/boneil/Desktop/Picture_out1/';
var copyOnly = true;

function getImageDate(imageFile, callback) {
  try {

      new ExifImage({ image : imageFile }, function (error, exifData) {
          if (error) {
              //console.error('error on file: %s -  %s'.red, imageFile, error);
              //just get the file date
              var stats = fs.statSync(imageFile);
              var d = moment(stats.ctime);
              console.info('WARN: used file date: %s for file: %s'.cyan, d.toString(), imageFile);
              if(callback) callback(null, d);
              return;
          }
          else {
                var datetime = exifData.exif.CreateDate.split(' ');
                datetime[0] = datetime[0].replace(/:/g, '-');
                var datestring = datetime.join(' ');
                var d = moment(datestring);

                if(callback) callback(null, d);
                return;
                //console.info('invalid at: ' + d.invalidAt());
          }
      });
  }
  catch (e) {
      console.log('Error: ' + error.message);
      if(callback) callback(e);
  }
}

function isSupportedFile(fileInfo) {
  var ext = getExtension(fileInfo).toLowerCase();

  return (supportedFileExtensions.indexOf(ext) !== -1);
}

function getExtension(filename) {
    var ext = path.extname(filename||'').split('.');
    return ext[ext.length - 1];
}

function mkdirsSync(dirPath) {

  var dirs = dirPath.split(path.sep);
	var root = "";

	while (dirs.length > 0) {
		var dir = dirs.shift();
		if (dir === "") {// If directory starts with a /, the first path will be an empty string.
			root = path.sep;
      continue;
		}
		if (!fs.existsSync(root + dir)) {
			fs.mkdirSync(root + dir);
		}
		root += dir + path.sep;
	}
}

function ensureDirectoryStructure(fileInfo) {
  var year = fileInfo.fileDate.year();

  //create the year path to check
  var yearPath = path.join(destinationPath, year.toString());

  //console.info('checking year path: %s'.cyan, yearPath);

  if(!fs.existsSync(yearPath)) {
    for (var i = 0; i < months.length; i++) {
      var month = months[i];
      mkdirsSync(path.join(yearPath, '/' + (i+1).toString() + '_' + month));
    }
  }

}

function doFilesMatch(sourceFilePath, targetFilePath) {
  //there is a file with the same name, is it the same file?
  var targetStat = fs.statSync(targetFilePath);
  var sourceStat = fs.statSync(sourceFilePath);

  //TODO: simple size compare for now...  need to maybe do more

  return targetStat.size === sourceStat.size;

}

function detectHandleCopyCollision(sourceFilePath, targetFilePath) {

    var results = {};

    //set some defaults
    results.verifiedFileName = targetFilePath;
    results.shouldCopy = true;

    //check if the target file already exists and see if it is the same file as the source
    if(!fs.existsSync(targetFilePath)) {
        return results;
    }
    else {



      if( doFilesMatch(sourceFilePath, targetFilePath) ) {
          console.info('SKIPPING: target file: %s already exists.'.cyan, targetFilePath);
          results.shouldCopy = false;
      }
      else {

          //update the name with a time stamp
          results.verifiedFileName = path.join(path.dirname(targetFilePath), path.basename(targetFilePath, path.extname(targetFilePath)) + '___collision___' + new Date().getTime() + path.extname(targetFilePath));
      }

      return results;

    }

}

function copyFile(source, target, cb) {

  console.info('copying %s to %s', source, target);

  var cbCalled = false;

  var rd = fs.createReadStream(source);
  rd.on("error", function(err) {
    done(err);
  });
  var wr = fs.createWriteStream(target);
  wr.on("error", function(err) {
    done(err);
  });
  wr.on("close", function(ex) {
    done();
  });
  rd.pipe(wr);

  function done(err) {
    if (!cbCalled) {
      cb(err);
      cbCalled = true;
    }
  }
}

function processFile(fileInfo, callback) {

  getImageDate(fileInfo.fullPathToFile, function (e, fileDate) {

    //console.info('file %s create year: %s month: %s', fileInfo.fullPathToFile, fileDate.year(), fileDate.month());

    //store the file date we need in the fileInfo
    fileInfo.fileDate = fileDate;

    //ensure the directory structure exists in the destination
    ensureDirectoryStructure(fileInfo);

    //copy the file to the directory structure
    var targetFilePath = path.join( destinationPath,
                                    fileInfo.fileDate.year().toString(),
                                    '/' + (fileInfo.fileDate.month() + 1) + '_' + months[fileInfo.fileDate.month()] + '/',
                                    fileInfo.fileName);


    var collisionResults = detectHandleCopyCollision(fileInfo.fullPathToFile, targetFilePath);

    if(collisionResults.shouldCopy) {

      targetFilePath = collisionResults.verifiedFileName;

      copyFile(fileInfo.fullPathToFile, targetFilePath, function(err) {

        if(err) {
          console.error('failed to copy file: %s, \nERROR: %s'.red, fileInfo.fullPathToFile, err);
        }
        else {
            //if verify is set then verify the copy and org files match
            if(!doFilesMatch(fileInfo.fullPathToFile, targetFilePath)) {
              err = 'files do not match after copy!!! source: ' + fileInfo.fullPathToFile + ' target: ' + targetFilePath;
            }

            //signal we are done with this file
            if(callback) callback(err, fileInfo);
            return;
        }
      });
    }
    else {
        if(callback) callback(e, fileInfo);
    }

  });
}

function processDirectories(source, destination, cponly) {

  file.walkSync(sourcePath, function(dirPath, dirs, files) {
    console.info('Walking tree: %s'.cyan, dirPath);
    if(files) {

      async.eachSeries(files, function (f, next) {
        var fullPathToFile = path.join(dirPath, f);

        if(fs.existsSync(fullPathToFile) && isSupportedFile(fullPathToFile)) {

          processFile({fullPathToFile : fullPathToFile, fileName : f}, function (error, fileInfo) {

            //if this is not a copy only run then delete the org file (if there are no errors)
            if(!error && !copyOnly) {
              fs.unlinkSync(fullPathToFile);
            }

            next(error);
          });

        }
        else {
          next();
        }
      },
      function (err) {
        //console.info('walking tree COMPLETE: %s', dirPath);
      });
    }
    else {console.error('no files'.red)}

  });
}

sourcePath = argv.s;
destinationPath = argv.d;
copyOnly = argv.c || true;

processDirectories();
