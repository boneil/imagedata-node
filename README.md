# Picture mover node script #

Script to read image metadata and move files into YYYY/Month/Picture

```
//pass these args to control the script when starting it.

sourcePath = argv.s;
destinationPath = argv.d;
copyOnly = argv.c || true;

//This is the function that kicks it all off.
processDirectories();

```